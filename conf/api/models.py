from django.db import models
from rest_framework import serializers


# Create your models here.
from diagnosisapp.models import (
    DiagnosisRecords,
)

class DiagnosisSerializer(serializers.ModelSerializer):
    class Meta:
        model=DiagnosisRecords
        fields = '__all__'

class AddDiagnosisSerializer(serializers.ModelSerializer):
    class Meta:
        model=DiagnosisRecords
        fields = '__all__'

class EditDiagnosisSerializer(serializers.ModelSerializer):
    class Meta:
        model=DiagnosisRecords
        fields = '__all__'

class CodeDiagnosisSerializer(serializers.Serializer):
    code=serializers.CharField()
    class Meta:
        fields = ('code')
