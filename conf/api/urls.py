from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework import routers
from . import views

from .views import (
	DiagnosisViewSets,
	)

router = routers.DefaultRouter()
router.register(r'diagnosis',DiagnosisViewSets,base_name='diagnosis')

urlpatterns = [
    url(r'^', include(router.urls)),
    ]
