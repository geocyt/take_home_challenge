from django.test import TestCase, Client
from rest_framework.test import APIRequestFactory
from diagnosisapp.models import DiagnosisRecords
from django.urls import reverse
from .models import DiagnosisSerializer
from api.views import DiagnosisViewSets
from rest_framework import status



client = Client()
a = DiagnosisViewSets()
# Create your tests here.
class DiagnosisTest(TestCase):
    def setUp(self):
        #test for creating DiagnosisRecord
        d1=DiagnosisRecords.objects.create(code='001',title='First diagnosis')
        d2=DiagnosisRecords.objects.create(code='002',title='Second diagnosis')
        d3=DiagnosisRecords.objects.create(code='003',title='Third diagnosis')

    # def test_diagnosis(self):
    #     diag_001=DiagnosisRecords.objects.get(code='001')
    #
    #     diag_002=DiagnosisRecords.objects.get(code='002')
    #
    #     self.assertEqual(diag_001.get_diagnosis(), '001 First diagnosis')
    #
    #     self.assertEqual(diag_002.get_diagnosis(), '002 Second diagnosis')

    def test_get_all_diagnosis(self):
        response =self.client.get(reverse('api-urls:diagnosis-list'))
        diags = DiagnosisRecords.objects.all()

        serializer = DiagnosisSerializer(diags, many=True)
        self.assertEqual(response.data['results'], serializer.data)

    def test_view_diagnosis_by_code(self):
        response=client.get(reverse('api-urls:diagnosis-list'))
        diag=DiagnosisRecords.objects.get(code='001')

        serializer=DiagnosisSerializer(diag, many=False)
        self.assertEqual(response.data['results'][0],serializer.data)

    def test_edit_diagnosis(self):

        data={'code':'001','title':'Measels'}

        response = client.put('api/diagnosis/edit_diagnosis/?code=001&title=Measels', kwargs={'code':'001','title':'Measels'},content_type='application/json')
        self.assertEqual(response.status_code,status.HTTP_404_NOT_FOUND)


    def test_delete_diagnosis(self):
        data={'code':'001'}
        # response=client.delete(reverse('api-urls:diagnosis-list'), data=data)
        response=client.post('/api/diagnosis/delete_diagnosis/', data={'code':'001'},content_type='application/json')

        try:
            diags=DiagnosisRecords.objects.all()
            len(diags)
        except Exception as e:
            print('sorry')

        # self.assertEqual(response.status_code,status.HTTP_200_OK)
        self.assertEqual(len(diags),2)
