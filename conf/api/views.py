from django.shortcuts import render
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from rest_framework.permissions import AllowAny
from rest_framework import viewsets
from rest_framework.decorators import list_route, detail_route,api_view
from rest_framework import pagination

#imports
from diagnosisapp.models import (
    DiagnosisRecords,
)
from .models import ( DiagnosisSerializer,
    CodeDiagnosisSerializer,
    AddDiagnosisSerializer,
    EditDiagnosisSerializer,
    # DeleteDiagnosisSerializer
)

# Create your views here.
class DiagnosisViewSets(viewsets.ModelViewSet):
    queryset = DiagnosisRecords.objects.all()
    serializer_class = DiagnosisSerializer
    permission_classes =  (permissions.AllowAny,)

    serializer_classes = {'add_diagnosis': AddDiagnosisSerializer,
                          'edit_diagnosis': EditDiagnosisSerializer,
                          'delete_diagnosis': CodeDiagnosisSerializer,
                          'view_diagnosis_by_code':CodeDiagnosisSerializer,
                          }
    def get_serializer_class(self):
        cls = super(DiagnosisViewSets, self).get_serializer_class()
        classes = getattr(self, 'serializer_classes', {})
        if classes:
            xs = [x for x in self.request.path.split('/') if x]
            endpoint = xs[-1]
            for k, v in classes.items():
                if k == endpoint:
                    cls = v
        return cls

    @list_route(methods=['get'])
    def view_all_diagnosis(self, request, *args, **kwargs):
        queryset = DiagnosisRecords.objects.order_by('code').all()
        page=self.paginate_queryset(queryset)
        if page is not None:
            serializer=DiagnosisSerializer(page,many=True)
            return self.get_paginated_response(serializer.data)

        serializer=DiagnosisSerializer(queryset,many=True)
        return paginator.get_paginated_response(serializer.data)

    @list_route(methods=['post'])
    def add_diagnosis(self,request, *args, **kwargs):
        r_code = request.data['code']
        r_title = request.data['title']
        result = {'status':403}

        obj=DiagnosisRecords.objects.create(code=r_code,title=r_title)
        if obj:
            data={'code':obj.code, 'title':obj.title}
            result = {'status':status.HTTP_200_OK, 'data':data}
            return Response(result)
        else:
            return Response(result)

    @list_route(methods=['post'])
    def delete_diagnosis(self,request, *args, **kwargs):
        serializer = CodeDiagnosisSerializer(data=request.data)
        if serializer.is_valid():
            r_code = serializer.validated_data['code']
            result = {'status':status.HTTP_404_NOT_FOUND,'data':'record not found'}
            try:
                res=DiagnosisRecords.objects.filter(code=r_code).delete()
                if DiagnosisRecords.objects.filter(code=r_code).count() == 0:
                    result= {'status':status.HTTP_200_OK, 'message':'record deleted'}
                    return Response(result)
                else:
                    return Response(result)
            except Exception as e:
                result = {'status':403,'message':'error deleting'}
                return Response(result)
        else:
            return Response("Bad Serializer")

    @list_route(methods=['post'])
    def view_diagnosis_by_code(self, request, *args, **kwargs):
        serializer=CodeDiagnosisSerializer(data=request.data)
        if serializer.is_valid():
            r_code = serializer.validated_data['code']
            try:
                diag=DiagnosisRecords.objects.get(code=r_code)
                serialized_obj=DiagnosisSerializer(diag)
                return Response(serialized_obj.data)
            except Exception as e:
                return Response({'status':status.HTTP_404_NOT_FOUND})

        else:
            return Response({'status_code':'Bad input format'})

    @list_route(['PUT'])
    def edit_diagnosis(self, request, *args, **kwargs):
        serializer = DiagnosisSerializer(data=request.data)
        print(request.data)

        if serializer.is_valid():
            r_code = request.data['code']
            r_title = request.data['title']
            result = {'status':status.HTTP_404_NOT_FOUND,'data':{}}
            try:
                res=DiagnosisRecords.objects.filter(code=r_code).update(title=r_title)
                print(res)
                if res==1:
                    obj=DiagnosisRecords.objects.get(code=r_code)
                    data={'code':obj.code, 'title':obj.title}
                    result= {'status':status.HTTP_200_OK, 'data':data}
                    return Response(result)
                else:
                    return Response(result)
            except Exception as e:
                return Response(result)
        else:
            return Response({'status_code':'Bad input format'})
