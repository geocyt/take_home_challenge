from django.db import models
import datetime


# Create your models here.
class DiagnosisRecords(models.Model):
    code = models.CharField(max_length=10,null=False)
    title = models.TextField()
    date_added=models.DateTimeField(auto_now_add=True, auto_now=False)
    date_modified=models.DateTimeField(auto_now_add=False, auto_now=True)

    def get_diagnosis(self):
        return self.code + ' '+ self.title

    def __repr__(self):
        return self.code + ' has been added'
