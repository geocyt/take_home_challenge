from django.contrib import admin
from diagnosisapp.models import DiagnosisRecords

# Register your models here.

class DiagnosisAdmin(admin.ModelAdmin):
    list_display = ['code','title','date_added','date_modified']
    search_fields = ['code']

admin.site.register(DiagnosisRecords,DiagnosisAdmin)
